ï»¿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;



namespace PDF_Encriptador
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnEjecutar_Click(object sender, EventArgs e)
        {
            try
            {
                ejecutarProcesoDeEncriptacion();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void ejecutarProcesoDeEncriptacion()
        {
            // 0) Obtener datos de Entrada
            string rutaOrigen = txtRutaOrigen.Text.ToString();
            string rutaDestino = txtRutaDestino.Text.ToString();

            //rutaOrigen = @"C:\temp\F572\SIT";
            //rutaDestino = @"C:\temp\F572\SIT\New folder";


            // 1) Leer los archivos .pdf que se encuentren en la "rutaOrigen"
            if (System.IO.Directory.Exists(rutaOrigen) == false)
                throw new Exception("La carpeta de Origen no es vÃ¡lida");

            if (System.IO.Directory.Exists(rutaDestino) == false)
                throw new Exception("La carpeta de Destino no es vÃ¡lida");


            string[] archivosRutaOrigen = System.IO.Directory.GetFiles(rutaOrigen);
            Array.Sort(archivosRutaOrigen, StringComparer.InvariantCulture);
            int cantTotalArchivos = archivosRutaOrigen.Length;
            int cantArchivosEncriptados = 0;

            // 2) Por cada archivo: obtener NroLegajo, generar clave, pdf encriptado y agregar al archivo de salida
            TextoPlano objTextoPlano = new TextoPlano(rutaDestino);
            objTextoPlano.agregarLineaATextoPlano("LEGAJO", "CONTRASEÃA");

            foreach (string nombreArchivo in archivosRutaOrigen)
            {
                //2.1) Validaciones correspondientes
                validarArchivoExtensionPDF(nombreArchivo);

                //string claveAleatoria = randomString(8);
                string legajo = obtenerNroLegajo(nombreArchivo);
                string claveAleatoria = randomStringByLegajo(5, legajo);
                PDFHandler.generarPDFEncriptado(nombreArchivo, rutaOrigen, rutaDestino, claveAleatoria);
                objTextoPlano.agregarLineaATextoPlano(legajo, claveAleatoria);
                cantArchivosEncriptados++;
            }


            // 3) Informar estado 
            string mensaje = String.Format("Se encriptÃ³ {0} de {1} archivos PDF", cantTotalArchivos, cantArchivosEncriptados);
            MessageBox.Show(mensaje, "Encriptador", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }



        #region Utils: Funciones 
        private string randomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            string claveGenerada = new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());


            return claveGenerada;
        }
        private string randomStringByLegajo(int length, string legajo)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            string claveGenerada = new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());

            claveGenerada = String.Format("{0}{1}_{2}", claveGenerada, legajo.Substring(legajo.Length - 4), getTimeStamp());

            return claveGenerada;
        }

        public string getTimeStamp()
        {
            string formatoFecha = "yyyyMMdd_HHmmssfff";//"yyyy-MM-dd HH:mm:ss.fff"
            //string timestamp = DateTime.UtcNow.ToString(formatoFecha, CultureInfo.InvariantCulture);
            string timestamp = DateTime.Now.ToString(formatoFecha, CultureInfo.CurrentCulture);
            return timestamp;
        }

        private string obtenerNroLegajo(string archivoLeidoOrigen)
        {
            // Obtener legajo del nombre archivo 
            string nombreArchivo = System.IO.Path.GetFileName(archivoLeidoOrigen);
            string[] arrayNombreArchivo = nombreArchivo.Split('_');
            string posLegajo = arrayNombreArchivo[1].ToString();
            if (posLegajo.ToString() == "")
                throw new Exception("El nombre de Archivo PDF debe ser liquidacion_legajo.pdf ");

            if (posLegajo.Split('.').Length > 1)
                posLegajo = posLegajo.Split('.')[0];

            return posLegajo;
        }


        private void validarArchivoExtensionPDF(string archivoLeido)
        {
            // Que sea un archivo con extensiÃ³n PDF
            string extensionArchivo = System.IO.Path.GetExtension(archivoLeido);
            if (extensionArchivo != ".pdf")
                throw new Exception("El archivo tiene que ser de extensiÃ³n PDF");
        }


        #endregion Utils: Funciones 
    }
}
