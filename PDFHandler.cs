ï»¿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Pdf.Security;
using System.Drawing;
using System.Diagnostics;
using System.IO;
using iTextSharp.text.pdf;

namespace PDF_Encriptador
{
    class PDFHandler
    {
        public void guardarPDFConClave(string clave)
        {
            /*
                        //Create PDF document
                        PdfDocument document = new PdfDocument();
                        //Add a page in the PDF document
                        PdfPage page = document.Pages.Add();
                        //Access the PDF graphics instance of the page
                        PdfGraphics graphics = page.Graphics;
                        //Create the PDF font instance
                        PdfStandardFont font = new PdfStandardFont(PdfFontFamily.TimesRoman, 20f, PdfFontStyle.Bold);
                        //PDF document security
                        PdfSecurity security = document.Security;
                        //Specifies encryption key size, algorithm, and permission
                        security.KeySize = PdfEncryptionKeySize.Key256BitRevision6;
                        security.Algorithm = PdfEncryptionAlgorithm.AES;
                        //Customize permission in the document
                        security.Permissions = PdfPermissionsFlags.Print | PdfPermissionsFlags.FullQualityPrint;
                        //Provide owner and user password
                        security.OwnerPassword = "SyncOPÃ256PDF";
                        security.UserPassword = "SyncUPâ¬99PDF";
                        //Draw text in PDF page
                        graphics.DrawString("Document is encrypted with AES 256 Revision 6", font, PdfBrushes.Black, PointF.Empty);
                        //Save the document to file system
                        document.Save("EncryptedDocument.pdf");
                        //Close the document
                        document.Close(true);
                        //This will open the PDF file so, the result will be seen in default PDF viewer
                        Process.Start("EncryptedDocument.pdf");
                        */
        }


        public static void generarPDFEncriptado(string nombreArchivo, string rutaOrigen, string rutaDestino, string clave)
        {
            /* https://stackoverflow.com/questions/370571/password-protected-pdf-using-c-sharp */


            string rutaOrigenArchivoPDF = nombreArchivo;
            string nombreArchivoOriginal = Path.GetFileName(nombreArchivo);
            //Genera los nombres de archivos
            string rutaDestinoArchivoPDF = rutaDestino + @"\" + nombreArchivoOriginal;

            using (Stream input = new FileStream(rutaOrigenArchivoPDF, FileMode.Open, FileAccess.Read, FileShare.Read))
            using (Stream output = new FileStream(rutaDestinoArchivoPDF, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                PdfReader reader = new PdfReader(input);
                PdfEncryptor.Encrypt(reader, output, true, clave, clave, PdfWriter.ALLOW_PRINTING);
            }
        }

    }
}
