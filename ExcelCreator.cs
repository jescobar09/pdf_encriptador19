ï»¿using System;
using System.Windows.Forms;

namespace PDF_Encriptador
{
    class ExcelCreator
    {

        private Microsoft.Office.Interop.Excel.Worksheet hoja;
        private Microsoft.Office.Interop.Excel.Application excel;
        private static int fila;

        public Microsoft.Office.Interop.Excel.Worksheet getHoja()
        {
            return hoja;
        }


        public ExcelCreator()
        {
            fila = 0;
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            //Microsoft.Office.Interop.Excel.Worksheet hoja;
            //Microsoft.Office.Interop.Excel.Application excel;
            excel = new Microsoft.Office.Interop.Excel.Application();
            excel.Application.Workbooks.Add(true);
            excel.Visible = true;

            hoja = (Microsoft.Office.Interop.Excel.Worksheet)excel.ActiveSheet;
            hoja.Activate();
            //foreach (DataGridViewColumn col in this.gridSalida.Columns)
            //  hoja.Cells[1, col.Index + 1] = col.HeaderText;
            hoja.Cells[1, 1] = "LEGAJO";
            hoja.Cells[1, 2] = "CONTRASEÃA";
        }
        public static void agregarLineaEnExcel(string legajo, string clave)
        {
           // hoja.Cells[fila, 1] = legajo;
            //hoja.Cells[fila, 2] = clave;

            fila++;
        }
        public void guardarExcel()
        {
            releaseObject(hoja);
            releaseObject(excel);
            //excel.Workbooks.Close();
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                //obj = null;
            }
            catch (Exception ex)
            {
                //obj = null;
                MessageBox.Show("Unable to release the Object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
