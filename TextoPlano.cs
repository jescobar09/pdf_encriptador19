ï»¿using System;
using System.IO;

namespace PDF_Encriptador
{
    class TextoPlano
    {
        #region Atributos \ Constructor
        private const string nombreArchivo = "clavesRecibosPDF.txt";
        public TextoPlano(string _rutaDestino)
        {
            rutaDestino = _rutaDestino;
            string directorio = getRutaDestino() + @"\CLAVES\";
            StreamWriter log;
            log = new StreamWriter(directorio + nombreArchivo, false);
            log.Close();
            log.Dispose();
        }

        private string rutaDestino;
        public void setRutaDestino(string _rutaDestino)
        {
            rutaDestino = _rutaDestino;
        }

        public string getRutaDestino()
        {
            return rutaDestino;
        }


        #endregion Atributos \ Constructor
        public void agregarLineaATextoPlano(string legajo, string clave)
        {
            StreamWriter log;
            string directorio = getRutaDestino() + @"\CLAVES\";
            //const string nombreArchivo = "clavesRecibosPDF.txt";

            System.IO.Directory.CreateDirectory(directorio);
            log = new StreamWriter(directorio + nombreArchivo, true);

            string mensaje = String.Format("{0}\t{1}", legajo, clave);
            log.WriteLine(mensaje);
            log.Close();
            log.Dispose();
        }

    }
}
